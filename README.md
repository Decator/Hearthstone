# Hearthstone
Martin Ars, Alexis Claveau, Alexis Loret, Maud Van Dorssen</br>
Université de Nantes, Master M1 ALMA

Welcome to our Hearthstone project. 

## Running the project
Begin with setting up the [database](https://gitlab.com/Decator/Hearthstone/tree/master/Database) using Docker.<br/>
Then, start the [engine](https://gitlab.com/Decator/Hearthstone/tree/master/Engine) using your favorite IDE.<br/>
Finally, start an Angular [client](https://gitlab.com/Decator/Hearthstone/tree/master/Client). 
